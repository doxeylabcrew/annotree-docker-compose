#!/bin/bash

read -p "Reloading database will erase all existing data, as well as stop all currently running db container, are you sure? [y/n]" -n 1 -r
echo # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	echo 'aborting'
  exit 0
fi

if [ ! -r .env ]; then
	echo 'cannot find .env file in the current folder, are you in docker-compose project directory?'
	exit 1
fi

# remove data volume
bash remove_data.sh &> /dev/null

# we can source .env file since it is valid bash
. .env
cd mysql;
sudo docker build -t annotree_database . || exit 1
cd ..;
echo 'downloading data from specified URL'
if [ ! -r import.sql.tar.gz ];then
	curl $database_dump_url -o import.sql.tar.gz.tmp && mv import.sql.tar.gz.tmp import.sql.tar.gz
	if [ ! $? ];then
		echo "download failed"; exit 1
	fi
fi


sudo docker volume create annotree-docker-compose_anno_tree_db || exit 1
sudo docker run --rm --mount type=bind,source=$(pwd),destination=/root -v annotree-docker-compose_anno_tree_db:/var/lib/mysql -i annotree_database bash <<EOM
set -o pipefail
service mysql start || exit 1
sleep 2
echo 'loading downloaded gz file to database'
[ -r /root/import.sql.tar.gz ] || exit 10
tar -vzxOf /root/import.sql.tar.gz | mysql -u root --default-character-set=utf8 || exit 1
echo "CREATE USER '$mysql_username'@'%' IDENTIFIED BY '$mysql_password';GRANT ALL PRIVILEGES ON *.* TO '$mysql_username'@'%'; FLUSH PRIVILEGES;" | mysql -u root --default-character-set=utf8

if [ ! $? ]; then
	service mysql stop
	echo "database loading failed"
	exit 1
fi

service mysql stop
echo 'finished loading database'
EOM
if [ ! $? ];then
	echo "loading mysql database failed"
	exit 1
fi

echo "finished loading database; note that it is normal to see ERROR 1045 (28000)."
