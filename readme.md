###Installation

>NOTE (Feb 1, 2019): as the project continue to evolve this file may be out of date. If you require assistance please contact the developers and we'd be happy to help.


Please be sure you have at least 130G of free diskspace, the database dump is quite large.
Since we are using docker, please also make sure you have sudo access to your machine.


1. Download this project, unzip it to a suitable directory
1. referring to `sample_env`, create a `.env` file in this folder
1. change `database_dump_url` in `.env` to the SQL download URL described in this [readme](https://bitbucket.org/doxeylab/annotree-database)
1. run `bash load_data.sh`, this may take a few hours to download, then another hour to load
1. run `sudo docker-compose up`
1. By default, the web page will be served from `http://localhost:80`, but if that is used for another purpose, you can change `docker-compose.yml`, change `80:80` to `<port on your host machine>:80`, usually 8080 is recommended for testing.

#Restarting service
1. run `sudo docker-compose down` to remove running containers, then `sudo docker-compose up`

#Uninstall
1. run `bash remove_data.sh` to remove all data, then `sudo docker-compose down` to remove all containers

###Troubleshoot
The installation has been tested in Ubuntu 16.04, try restarting the service if you run into problems. Make sure

* create a `.env` file in this directory
* internet connection is stable and all data are downloaded
* check if there is port conflict, is port 80 on your machine in use?
* if all else failed, email the corresponding authors with full log of installation, there is always help
